#! /bin/bash

luks_clone_setup () {

	#in case there is no mention of lvm, first command won't only return nothing, it will also throw an error and stop the script
	test_lvm=$(lsblk | grep 'lvm') || test_lvm=""

	if [[ -n $test_lvm ]];then

		lvm_get_volumes
		luks_reencrypt

		extend_lvm_mute
		# here we won't ask questions like in the regular autoextend script :
		# we assume that we want to extend to all available space
		# (which should exist since the image is smaller than 250GB)

	else
	
		error_non_lvm
		
	fi	

	reboot_oem
}


error_non_lvm () {
	
	DSM "\nPartitionnement classique détecté (non-LVM)\n>>> Sortie du programme d'installation de clone LUKS\n\n"
			
}

lvm_get_volumes () {

	#get LVM root logical volume
	lv_query_result=($(sudo lvs | grep -E 'root' | awk '{printf "%s %s %s ", $1, $2, $3}'))
	lv=${lv_query_result[0]}
		vg=${lv_query_result[1]}
		#get corresponding physical volume
	pv_query_result=($(sudo pvs | grep -E "$vg" | awk '{printf "%s %s %s ", $1, $2, $3}'))
	pv=${pv_query_result[0]}
	
	device=$pv
	part=$device
	disk_format_error=false
	#multiple_roots=false #not used yet
	
	
	if [[ $pv == *"crypt"* ]];then
		crypt=true
	else
		crypt=false
	fi
	
	DSU "\nPartitionnement LVM détecté :"
	DSU "\n > Volumes LVM :\n"
	DSU " - volume logique sélectionné : "$lv
	DSU " - groupe de volumes correspondant : "$vg
	DSU " - volume physique correspondant : "$pv"\n"
	
	
	if [[ $device == *"mapper"* ]];then
		device=($(echo $device | cut -d '/' -f 4)) 
		device='/dev/'$device
		# /dev/mapper/quelquechose --> /dev/quelquechose
		#$device will be used as an argument for parted to resize the physical part, 
		#so we want to point towards the actual device and not the LUKS/LVM object referenced in /dev/mapper/
	fi
	
	if [[ $device == *"nvme"* ]];then
		#typically nvme parts are written as such : nvmeXnXpX (nvme0n1p3, nvme1n2p1...)
		#thus to get the device name we want to remove everything starting from the last 'p' : nvme0n1p3 --> nvme0n1
		#in case of an encrypted disk, the first line is also needed: nvme0n1p3_crypt -->nvme0n1p --> nvme0n1
		device=${device%[0-9]*} 
		device=${device%p*} 
		DSU "NVMe détecté, disque : $device \n"
	elif [[ $device == *"sd"* ]];then
		#typically sd parts are written as such : sdAX (sdc1, sdb3...)
		#thus to get the device name we want to remove everything starting from the last number
		device=${device%[0-9]*} 
		DSU "SD détecté, disque : $device \n"
	elif [[ $device == *"hd"* ]];then
		#typically sd parts are written as such : hdAX (hdc1, hdb3...)
		#thus to get the device name we want to remove everything starting from the last number
		device=${device%[0-9]*} 
		DSU "HD détecté, disque : $device \n"
	else
		printf "\n////////////////////////////\nQuelque chose s'est mal passé : nom du disque incorrect ($device), sortie du programme d'extension\n////////////////////////////\n"
		device=''
		disk_format_error=true
	fi

}

luks_reencrypt () {
	
	if [[ -n $device && $disk_format_error == false && $crypt==true ]];then
		physical_part=$pv

		# for cases like 'nvme0n1p3_crypt', remove every non-digit character as long as there is a non-digit character at the end of the word
		while [[ $physical_part =~ .*[^0-9]+$ ]];do # --> regexr.com
			physical_part=${physical_part%[^0-9]} 
		done
		# at this point the result would be $physical_part=/dev/mappper/nvme0n1p3 but we only want /dev/nvme0n1p3
		# so we keep everything after the last '/', then we put that after '/dev/' :
		if [[ $physical_part == *"mapper"* ]];then
			physical_part='/dev/'${physical_part##*/} 
		fi

		DS "\nRe-chiffrement de la partition ${physical_part}, cela peut prendre un peu de temps ...\n"
		sudo cryptsetup reencrypt $physical_part && checkL
	else
		printf "\nRe-chiffrement : erreur !"
	fi

}


extend_lvm_mute () {

	
	if [[ -n $device && $disk_format_error == false ]];then #stop if value is empty #+add multiple_roots check, if it is to be implemented
	
		part_number=$pv
		#for cases like 'nvme0n1p3_crypt', we begin by removing every non-digit character as long as there is a non-digit character at the end of the word
		while [[ $part_number =~ .*[^0-9]+$ ]];do # --> regexr.com
			part_number=${part_number%[^0-9]} 
		done
		part_number=${part_number##*[a-z]} #then we remove characters before the part number, i.e. we remove the longest sequence ending with a lowercase letter
		free_space_query_result=($(sudo parted $device print free -m | awk '{printf "%s ", $1}')) 
		free_space=""
		
		for i in "${!free_space_query_result[@]}"; do 
			
			#to choose if we display next part's size as 'free space' : 2 tests : 
			#is the part number right ? is the next part an actual part (ie. not #1) ?
			#for insight, check the output of : sudo parted $device print free -m
			line_starting_number=($(echo ${free_space_query_result[i]} | cut -d ':' -f 1))
			
			#setting a default value if the next line doesnt exist, otherwise the script will stop as it will eventually point to an undefined value
			following_line_starting_number=($(echo ${free_space_query_result[i+1]:-'99999::::::::'} | cut -d ':' -f 1)) 
			
			if [[ $line_starting_number = $part_number && $following_line_starting_number = 1 ]];then
			
				#double checking if the next part is actually 'free'
				double_check_query=($(echo ${free_space_query_result[i+1]} | cut -d ':' -f 5))
				
				if [[ $double_check_query == *"free"* ]];then
				
					free_space=($(echo ${free_space_query_result[i+1]} | cut -d ':' -f 4 )) 
					#4th column = part's size : in our case : free space
					DS "\nEspace non alloué trouvé après la partition $pv ($lv) : ${RESETCOLOR} $free_space\n"
					## printf "Voulez vous étendre le volume $lv sur cet espace (o)? \nOu bien laisser les partitions ainsi (n)? " && read answerExt
					if [[ -n $free_space ]];then
					
						#'growpart' can do all of this in a single line, but wanted to keep the script usable without additional installations
					
						hypothetical_next_part=${part##*/}
						hypothetical_next_part=${hypothetical_next_part%[0-9]*}
						hypothetical_next_part+=$((part_number+1))
						next_part_info_path='/sys/block/'${device##*/}'/'$hypothetical_next_part
						
						
						#in the case where there is a part after existing adjacent free space, 
						#we cannot simply resize to 100% : we must know the precise distance between parts
						if [[ -d $next_part_info_path ]];then
							DSU "\nPartition existante au delà de l'espace libre : $hypothetical_next_part, extension jusqu'à celle-ci ! \n"
							next_part_start=$(($(cat $next_part_info_path/start)-40)) # -40 = safety offset
							
							sudo parted $device unit s resizepart $part_number $next_part_start && checkL
						else
							#extending the physical partition
							DSU "\nExtension de la partition $part sur l'ensemble du disque $device :"
							sudo parted -s $device resizepart $part_number 100% && checkL
						fi
					else
						printf "\n > Abandon de l'extension physique\n\n"
					fi
				fi
			fi
			done
			
			if [[ -z $free_space ]];then
				DS "\nPas d'espace physique libre trouvé après la partition $pv ($lv) : fin de l'extension physique\n" 	
			fi

			### LVM-LUKS specific manipulations below :
			# this code block is located outside of the
			# TODO : implement free space detection at the LVM level (compare outputs from 'lvs' and 'parted print free -m')
						
			if [[ $crypt ]];then
			#additional resizing of the ciphered volume (on which the LVM physical volume is implemented)
				DS "\nExtension du volume LUKS"
				sudo cryptsetup resize $pv && checkL
			else 
				printf "\nInstallation non-chiffrée !\n"
			fi
			
			#extending the LVM physical volume on the whole physical volume
			sudo pvresize $pv && checkL
			
			#extending the LVM logical volume on the whole volume group
			sudo lvextend -l +100%FREE --resizefs /dev/mapper/$vg-$lv && checkL
		
		
	elif [[ $disk_format_error == true ]];then 
	#disk name is not in either 'nvme' 'sd' or 'hdd' formats
		printf "\n////////////////////////////\nQuelque chose s'est mal passé : variable 'device' nulle, sortie du programme d'extension\n////////////////////////////\n"
		
	fi
		
}
