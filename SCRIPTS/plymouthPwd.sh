#!/bin/bash

# TEST FILE, not for production use !


# first check that we are in an appropriate runlevel
#echo $(runlevel)
[[ $EUID -ne 0 ]] && echo -e "This script must be run as root." && exit 1

echo -e "Testing plymouth default theme ..." && plymouthd && sleep 1

# check if the plymouthd daemon is alive
plymouth --ping
if [[ $? -eq 1 ]]; then
    echo -e "ERROR: Plymouth daemon not running" && exit 1
fi

# show the default splash screen for 2 seconds and password
plymouth  --show-splash && sleep 2
pwdPrompt="Veuillez saisir votre mot de passe de chiffrement"
plymouth ask-for-password --prompt="${pwdPrompt}" && sleep 1

# using a command rather than an option
# plymouth ask-question --prompt="What is your name?" && sleep 5

plymouth --quit && echo -e "Done ..." && exit 0

##############################################################################################
# The two main binaries involved in Plymouth are /sbin/plymouthd, a daemon that does most of 
# the work by displaying the splash screen and logging the boot session, and /bin/plymouth 
# which is the interface to /sbin/plymouthd. Both have a number of useful options : 

# /sbin/plymouthd --help
# /bin/plymouth --help
##############################################################################################