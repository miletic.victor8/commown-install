#!/bin/bash

# Get current script directory
currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


function install_dependencies() {
  # The convert utility is used by grub2-themes
  # and x11-utils is used for xdpyinfo
  apt update && apt install -y imagemagick x11-utils
}


function configure_grub() {
  # Unarchive grub2-themes, copy needed data into the resulting dir
  # (custom background and custom grub config)
  # then execute the grub theme install script
  grubDir=$1
  resolution=$2
  cd /tmp
  tar xf ${grubDir}/grub2-themes.tar
  cd grub2-themes
  cp ${grubDir}/backgroundCommown_grub.jpg ./custom-background.jpg
  cat ${grubDir}/etc_default_grub.txt | sed "s:__RESOLUTION__:$resolution:" > /etc/default/grub

  bash install.sh -4 -l -C -w
}


function fix_clock() {
  # Fix hour different from windows (cannot be fixed in Win 10)
  timedatectl set-local-rtc 1 --adjust-system-clock
}

multiboot_install () {
	install_dependencies
	configure_grub $currentDir $(xdpyinfo | awk '/dimensions/ {print $2}')
	fix_clock
	
	# this change of location allows GRUB to display the theme in case the installation is cyphered :
	# at this point the root disk part is not unlocked, so the regular /usr/share/grub/themes folder is not readable
	if [[ ! -d /boot/grub/themes/Slaze ]];then 
		sudo mkdir -p /boot/grub/themes/Slaze 
	fi
	sudo cp -pr /usr/share/grub/themes/Slaze/* /boot/grub/themes/Slaze && checkL
	
	
	# returning to initial directory, otherwise next scripts won't work
	cd $grubDir
	cd ../..

}

