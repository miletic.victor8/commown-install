#! /bin/bash

# Use checkL only, to check the return values and prompt (extending disk is sensitive!)
#lsblk -f OR lsblk -io KNAME,TYPE,SIZE,MODEL # other ways to list disks

extend() {
    # Extending disk
    DS "Extending disk... \n"
    # "res" requires include.sh in main script
    local res=$(getDisk)              # if function returns instead of echo, use $?
    disk=$res && dname=${res#"/dev/"} # sdb # remove /dev/ from disk name (disk to extend)

    lsblk -e7 # exclude list 7 (loop) # check with "lsblk", you get MAJ : 7

    if [ -n "${disk}" ]; then # if there is a disk to extend
        DSBU "\n The extending disk is \"${dname}\" \"from ${disk}\"."
    else
        DSBU "\n There is no disk found to extend. You can still check with the disks listed above."
        printf 'Skip/quit extend (y/n)? ' && read answerExt
        if [ "$answerExt" != "${answerExt#[Yy]}" ]; then
            DS "Extending DONE, no disk found."
            return 0 # ou string empty
        fi
    fi

    printf 'Do you want to extend this (y)? / Chose another disk (n)? ' && read answerExt
    if [ "$answerExt" != "${answerExt#[Nn]}" ]; then # "#[]", any Y or y in 1st position will be dropped.
        printf 'Chose a disk listed above (Example : sdb): ' && read newDisk
        disk="/dev/$newDisk" && dname=${newDisk}
    fi

    DSBU "\n Extending disk \"${dname}\" \"from ${disk}\"."
    # Ask user to cipher disk with LUKS
    printf 'Cipher disk with LUKS (y/n)? ' && read answerDisk
    if [ "$answerDisk" != "${answerDisk#[Yy]}" ]; then # "#[]", any Y or y in 1st position will be dropped.
        DS "Formatting disk $dname with LUKS..."
        cryptsetup luksFormat ${disk} && checkL # format disk with luks
        DS "Opening disk $dname with LUKS..."
        cryptsetup luksOpen ${disk} ${dname}_crypt && checkL # ${disk}_crypt = luks name
        echo ${dname}_crypt $(blkid -o export ${disk} | grep UUID) none luks,discard >>/etc/crypttab
        disk="mapper/${dname}_crypt"
    else
        disk=${dname}
    fi

    DS "\n Extending disk $disk..."
    pvcreate /dev/${disk} && checkL
    vgextend vgubuntu /dev/${disk} && checkL
    lvextend /dev/mapper/vgubuntu-root /dev/${disk} -r && checkL
    DS "Extending DONE"
}

# Separate the /home directory to facilitate major updates with complete
# reinstallations without any loss of user data
home() {
    # Separating /home directory
    DS "Separating /home directory..."
    echo -e "Requires Debian equivalence"
    DS "Separating DONE"
}
