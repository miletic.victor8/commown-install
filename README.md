[<img src="IMAGES/logoCommown_big.png" width="200">](https://commown.coop/)

# Install


Commown's computer install scripts.<br>
--> [version number](version.txt)

## Installation process 

```bash
### GIT and Script installation
sudo -- sh -c 'apt update; apt upgrade -y; apt install -y git' # -y avoid to answer to all prompts 

git clone https://gitlab.com/miletic.victor8/commown-install install/
sudo bash ./install/commownScript.sh```
```
Add '-h' option to display help :
```bash
 ./install/commownScript.sh -h
```
Syntaxe : sudo bash ./install/commownScript.sh -[u|g|c|e|h]<br>
--> Ajouter une (seule) option pour automatiser les choix :

- u   Installation 'Ubuntu classique' : main, thème GRUB, Plymouth
- g   Installation 'PopOS gaming' : main, Plymouth, apps gaming
- c   Re-chiffrement & extension d'une installation chiffrée LVM-LUKS
- e   Auto-extension de la partition root dans espace non-alloué adjacent
- h   Afficher ce message


## Help

> You can refer to the [documentation](docs/Documentation script d'installation Linux- commown-install v1.0.0.pdf)

<br>

## Join us

### On our [website](https://commown.coop/)!
### On our social networks : &emsp; &darr; &emsp; <img src="https://media.giphy.com/media/3V52SV0C5mnDKGVZmU/giphy.gif" width="60"> &darr; <br><br>
<a href="https://www.linkedin.com/company/commown/?originalSubdomain=fr">
    <img src="https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white">
</a>
<a href="https://www.instagram.com/commown/?hl=fr">
    <img src="https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white">
</a>
<a href="https://twitter.com/commownfr?lang=fr">
    <img src="https://img.shields.io/badge/Twitter-%231DA1F2.svg?style=for-the-badge&logo=Twitter&logoColor=white">
</a>
<a href="https://mamot.fr/@commownfr">
    <img src="https://img.shields.io/badge/mastodon-%235640d5.svg?style=for-the-badge&logo=mastodon&logoColor=white">
</a>
<a href="https://www.facebook.com/commownfr">
    <img src="https://img.shields.io/badge/facebook-%23007be9.svg?style=for-the-badge&logo=facebook&logoColor=white">
</a>
<br><br>

## Logo 

<br>

Origin of the bash logo [commown_text](https://patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=Commown).

<br>

## *Licenses*

<br>

[![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/) 
[![Markdown](https://img.shields.io/badge/markdown-%23000000.svg?style=for-the-badge&logo=markdown&logoColor=white)](https://www.markdownguide.org/) 
[![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)](https://www.man7.org/linux/man-pages/man1/bash.1.html)
<br>
<br>
[![licensebuttons by-nd](https://licensebuttons.net/l/GPL/2.0/88x62.png)](https://creativecommons.org/licenses/by-nd/4.0)

[//]: # (IMPORTANT : https://github.com/Ileriayo/markdown-badges)
